require 'base64'

module Sass::Script::Functions
    def image_url(path, only_path = Sass::Script::Bool.new(false))
        $SizeLimit = 3*2**10 # If image size is <= 3 KB, it shall be inlined..
        assert_type path, :String

        _dir = File.dirname(options[:filename].to_s) # Directory in which the current file is in..
        _absolute_path = "#{_dir}/#{path.value}"

        _file_exists = File.readable? _absolute_path
        if (!_file_exists)
            puts "File not found or cannot be read: #{_absolute_path}"
            raise "File not found or cannot be read: #{_absolute_path}"
        end

        _file_size = File.size _absolute_path
        if (_file_size > $SizeLimit)
            _image_path = "#{path.value}"
        else
            File.open(_absolute_path) do |image|
                _image_data = Base64.encode64(image.read).gsub(/\n/,'')
                _image_path = "data:image/#{_absolute_path.split('.').last};base64,#{_image_data}"
            end
        end

        if only_path.to_bool
            Sass::Script::String.new(_image_path)
        else
            Sass::Script::String.new("url(#{_image_path})")
        end
    end
end
