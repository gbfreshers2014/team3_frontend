/*jslint nomen:true, node:true*/
'use strict';

var express = require('express'),
    libmojito = require('mojito'),
    cookieParser = require('cookie-parser'),
    bodyParser = require('body-parser'),
    session = require('express-session'),
    methodOverride = require('method-override'),
    flash = require('connect-flash'),
    passport = require('./authentication').passport,
    app,
    bypassLogin = false;

app = express();

// Set the port to listen on.
app.set('port', process.env.PORT || 8663);

// Create a new Mojito instance and attach it to `app`.
// Options can be passed to `extend`.
libmojito.extend(app, {
    context: {
        environment: "development"
    }
});

app.use(cookieParser());
app.use(bodyParser());
app.use(methodOverride());
app.use(session({ secret: 'gwn dog start' }));
// Initialize Passport!  Also use passport.session() middleware, to support
// persistent login sessions (recommended).
app.use(flash());
app.use(passport.initialize());
app.use(passport.session());


// Load the built-in middleware or any middleware
// configuration specified in application.json
app.use(libmojito.middleware());



// Load routes configuration from routes.json
app.mojito.attachRoutes();

app.get('/', libmojito.dispatch("loginFrame.index"));

app.post('/', passport.authenticate('local', { failureRedirect: '/?attempt=1', failureFlash: true }),
    function(req, res) {
   // console.log(req.user);
    //validation here
    if(req.user.role==='admin')
    {
        res.redirect('/dashboard');
    }
    else if(req.user.role==='employee') {
        res.redirect('/?attempt=2');
        res.end('Only Admin can login, please try again');
    }
    else
    {
        res.end('Server error, please try again');
    }
});

if( bypassLogin ) {
    app.get('/inventoryList/inventory', libmojito.dispatch("addInventoryFrame.index"));
    app.post('/inventoryList/inventory', libmojito.dispatch("addInventory.addItem"));
    app.get('/dashboard', libmojito.dispatch("dashboardFrame.index"));
    app.get('/inventoryList', libmojito.dispatch("inventoryListFrame.index"));
    app.post('/inventoryList', libmojito.dispatch("inventoryList.availabilityFilter"));
    app.get('/inventoryList/inventory/:id', libmojito.dispatch("inventoryList.inventoryDetailsPopup"));
//app.get('/inventoryDetails', libmojito.dispatch("inventoryDetailsFrame.index"));
//app.get('/employeeInventorySearch', passport.ensureAuthenticated, libmojito.dispatch("employeesInventoryFrame.index"));
    app.post('/inventoryList/inventoryDetail/job', libmojito.dispatch("inventoryList.assignItem"));
    app.get('/employeeInventoryList', libmojito.dispatch("employeesInventoryFrame.index"));
    app.post('/employeeInventoryList', libmojito.dispatch("employeesInventoryFrame.index"));
    app.post('/inventoryList/inventoryDetail/job/validation', libmojito.dispatch("inventoryList.employeeValidation"));

}
else {
//app.post('/login', libmojito.dispatch("login.authenticate"));
    app.get('/inventoryList/inventory', passport.ensureAuthenticated, libmojito.dispatch("addInventoryFrame.index"));
    app.post('/inventoryList/inventory', passport.ensureAuthenticated, libmojito.dispatch("addInventory.addItem"));
    app.get('/dashboard', passport.ensureAuthenticated, libmojito.dispatch("dashboardFrame.index"));
    app.get('/inventoryList', passport.ensureAuthenticated, libmojito.dispatch("inventoryListFrame.index"));
    app.post('/inventoryList', passport.ensureAuthenticated, libmojito.dispatch("inventoryList.availabilityFilter"));
    app.get('/inventoryList/inventory/:id', passport.ensureAuthenticated, libmojito.dispatch("inventoryList.inventoryDetailsPopup"));
//app.get('/inventoryDetails', libmojito.dispatch("inventoryDetailsFrame.index"));
//app.get('/employeeInventorySearch', passport.ensureAuthenticated, libmojito.dispatch("employeesInventoryFrame.index"));
    app.post('/inventoryList/inventoryDetail/job', passport.ensureAuthenticated, libmojito.dispatch("inventoryList.assignItem"));
    app.get('/employeeInventoryList', passport.ensureAuthenticated, libmojito.dispatch("employeesInventoryFrame.index"));
    app.post('/employeeInventoryList', passport.ensureAuthenticated, libmojito.dispatch("employeesInventoryFrame.index"));
    app.post('/inventoryList/inventoryDetail/job/validation', passport.ensureAuthenticated, libmojito.dispatch("inventoryList.employeeValidation"));
}

app.get('/logout', function(req, res){
    req.logout();
    res.redirect('/');
});

app.listen(app.get('port'), function () {
    console.log('Server listening on port ' + app.get('port') + ' ' +
                   'in ' + app.get('env') + ' mode');
});
