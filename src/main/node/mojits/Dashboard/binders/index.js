/*jslint anon:true, sloppy:true, nomen:true*/
/**
 * Copyright 2013 GwynnieBee Inc.
 */


/*global YUI,e*/
YUI.add('dashboard-binder-index', function(Y, NAME) {

/**
 * The dashboard-binder-index module.
 *
 * @module dashboard-binder-index
 */

    /**
     * Constructor for the DashboardBinderIndex class.
     *
     * @class DashboardBinderIndex
     * @constructor
     */
    Y.namespace('mojito.binders')[NAME] = {

        /**
         * Binder initialization method, invoked after all binders on the page
         * have been constructed.
         */
        init: function(mojitProxy) {
            this.mojitProxy = mojitProxy;
        },

        bind: function(node) {
            var me = this;
            this.node = node;
        }

    };

}, '0.0.1', {requires: ['event-mouseenter', 'mojito-client']});
