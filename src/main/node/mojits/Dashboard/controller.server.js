/*jslint anon:true, sloppy:true, nomen:true*/
/**
 * Copyright 2013 GwynnieBee Inc.
 */


/*global YUI*/
YUI.add('dashboard', function(Y, NAME) {

/**
 * The dashboard module.
 *
 * @module dashboard
 */

    /**
     * Constructor for the Controller class.
     *
     * @class Controller
     * @constructor
     */

    Y.namespace('mojito.controllers')[NAME] = {

        /**
         * Method corresponding to the 'index' action.
         *
         * @param ac {Object} The ActionContext that provides access
         *        to the Mojito API.
         */

        index: function(ac) {
            var user= ac._adapter.req.user.emailId;
            //console.log("user==="+ user);

            ac.models.get('model').getprofile(user, function(err, response)
            {
                if(err)
                {
                    ac.done({msg:"Error"},"json");
                }
                else {

                    if(response) {
                        //console.log(response.view, "---contr");
                        var view = JSON.parse(response._resp.responseText);
                        var data = {
                            fname: view.details.firstName, email: view.details.emailId, lname: view.details.lastName, dob: view.details.dateOfBirth,
                            doj: view.details.dateofJoining, bg: view.details.bloodGroup, sky: view.details.skypeId, phonenumber: view.details.phoneNo,
                            role: view.details.role, status: view.details.addresses.status
                        };
                        //ac.data.setAttrs({fname:response.frstname});
                        ac.data.setAttrs({fname: view.details.firstName, email: view.details.emailId, lname: view.details.lastName, dob: view.details.dateOfBirth,
                            doj: view.details.dateofJoining, bg: view.details.bloodGroup, sky: view.details.skypeId, phonenumber: view.details.phoneNo,
                            role: view.details.role, status: view.details.addresses.status});

                        ac.done(data, "index");
                    }
                    else{
                        ac.done({},"");
                    }

                    //ac.done({},"");
                }
            });
        }

    };

}, '0.0.1', {requires: ['mojito', 'mojito-assets-addon','mojito-data-addon',
    'mojito-models-addon', "mojito-params-addon","mojito-http-addon"]});
