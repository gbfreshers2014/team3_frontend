/*jslint anon:true, sloppy:true, nomen:true*/
/**
 * Copyright 2013 GwynnieBee Inc.
 */


/*global YUI*/

YUI.add('dashboard-model', function(Y, NAME) {

    var CONFIG = require("config"),
        url = CONFIG.endpoints.profile;
/**
 * The dashboard-model module.
 *
 * @module dashboard
 */

    /**
     * Constructor for the DashboardModel class.
     *
     * @class DashboardModel
     * @constructor
     */
    //var profileURL = "http://induction-dev.gwynniebee.com:8080/employee_information_management_t2-v1/userprofiles/";
    var profileURL = CONFIG.endpoints.profile;
    Y.namespace('mojito.models')[NAME] = {

        init: function(config) {
            this.config = config;
        },

        /**
         * Method that will be invoked by the mojit controller to obtain data.
         *
         * @param callback {function(err,data)} The callback function to call when the
         *        data has been retrieved.
         */
        getData: function(callback) {
            callback(null, { some: 'data' });
        },

        getprofile: function(user,callback) {
            Y.mojito.lib.REST.GET(profileURL+user+".json",{}, {
                    headers: {"Content-Type": "application/json"},
                    timeout: 12000
                },
                function (err, response) {
                    if(response) {
                        callback(null, response);
                    }
                    else {
                        callback(null,null);
                    }
                }
            );
        }

    };

}, '0.0.1', {requires: ['config']});
