/**
 * Copyright 2013 Gwynnie Bee Inc.
 */

/*global YUI*/
YUI.add('FrameMojit', function (Y, NAME) {

    'use strict';

    Y.namespace('mojito.controllers')[NAME] = {

        index: function (ac) {
            this.__call(ac);
        },

        __call: function (ac) {
            var config = ac.config.get();
            var children = config.children;
            for (var key in children) {
                if (children.hasOwnProperty(key)) {
                    children[key].action = children[key].action || ac.action;
                }
            }

            ac.composite.execute(config, function (data, meta) {

                // meta.assets from child should be piped into
                // the frame's assets before doing anything else.
                ac.assets.addAssets(meta.assets);

                if (ac.config.get('deploy') === true) {
                    ac.deploy.constructMojitoClientRuntime(ac.assets,
                        meta.binders);
                }

                // 1. mixing bottom and top fragments from assets into
                //    the template data, along with title and mojito version.
                // 2. mixing meta with child metas, along with some extra
                //    headers.
                ac.done(
                    Y.merge(data, ac.assets.renderLocations(), {
                        title: ac.config.get('title')
                    }),
                    Y.merge(meta, {
                        http: {
                            headers: {'content-type': 'text/html; charset="utf-8"'}
                        },
                        view: {name: 'index'}
                    })
                );
            });
        }
    };

}, '0.1.0', {requires: [
    'mojito',
    'mojito-assets-addon',
    'mojito-deploy-addon',
    'mojito-config-addon',
    'mojito-composite-addon',
    'mojito-params-addon'
]});
