/*jslint anon:true, sloppy:true, nomen:true*/
/**
 * Copyright 2013 GwynnieBee Inc.
 */


/*global YUI*/
YUI.add('addinventory-model-addItem', function(Y, NAME) {

    //var my_request = require("request"),
     var CONFIG = require('config');


    Y.namespace('mojito.models')[NAME] = {

        init: function(config) {
            this.config = config;
        },

        /**
         * Method that will be invoked by the mojit controller to obtain data.
         *
         * @param callback {function(err,data)} The callback function to call when the
         *        data has been retrieved.
         */
        getData: function(callback) {
            callback(null, { some: 'data' });
        },

        addItem: function(typeP, nameP, descP, user, callback) {
           var url = CONFIG.endpoints.addI;
            /*
            my_request({
                //url: "http://192.168.1.108:8080/a-1.0.0-SNAPSHOT/authenticate/" + un + ".json",
                url: url,
                method: "POST",
                body: {
                    "itemType": typeP,
                    "itemName": nameP,
                    "description": descP,
                    "createdBy": user
                },
                json: true

            }, function (err, response) {

                if(response) {
                    console.log("in model response = " + response.body);
                    callback(null, {res: response.body});
                }
                else{
                    callback(null,null);
                }

            });
            */
            Y.mojito.lib.REST.POST(url, JSON.stringify({
                "itemType": typeP,
                "itemName": nameP,
                "description": descP,
                "createdBy": user
            }), {
                headers: {"Content-Type": "application/json"},
                timeout: 12000
            }, function (err, response) {
                if(response) {
                    callback(null, response);
                }
                else {
                    callback(null,null);
                }
            });

        }

    };

}, '0.0.1', {requires: ['config']});
