/*jslint anon:true, sloppy:true, nomen:true*/
/**
 * Copyright 2013 GwynnieBee Inc.
 */


/*global YUI*/
YUI.add('addinventory', function(Y, NAME) {

/**
 * The addinventory module.
 *
 * @module addinventory
 */

    /**
     * Constructor for the Controller class.
     *
     * @class Controller
     * @constructor
     */
    Y.namespace('mojito.controllers')[NAME] = {

        /**
         * Method corresponding to the 'index' action.
         *
         * @param ac {Object} The ActionContext that provides access
         *        to the Mojito API.
         */
        index: function(ac) {
            /*ac.models.get('model').getData(function(err, data) {
                if (err) {
                    ac.error(err);
                    return;
                }*/
                //ac.assets.addCss('./index.css');
                ac.done();
            //});
        },

        addItem: function(ac) {
            var typeP = ac.params.getFromBody("typeP"),
                nameP = ac.params.getFromBody("nameP"),
                descP = ac.params.getFromBody("descP");

            var user= ac._adapter.req.user.emailId;
            //var user = "shikha";

            ac.models.get('addItem').addItem(typeP, nameP, descP, user, function(err, response)
            {
                if(err) {
                    ac.done({msg:"Error"},"json");
                }
                else {
                    if (response) {
                        ac.done({body: response._resp.responseText}, "json");
                    }
                    else {
                        ac.done({}, "");
                    }

                }
            });
        }

    };

}, '0.0.1', {requires: ['mojito', 'mojito-assets-addon', 'mojito-models-addon', 'dump', 'mojito-params-addon']});
