/*jslint anon:true, sloppy:true, nomen:true*/
/**
 * Copyright 2013 GwynnieBee Inc.
 */
/*global YUI*/
YUI.add('addinventory-binder-index', function(Y, NAME) {

    /**
 * The addinventory-binder-index module.
 *
 * @module addinventory-binder-index
 */
    var confirmDialog = new Y.Panel({
        contentBox : Y.Node.create('<div id="dialog" />'),
        bodyContent: '<div class="message icon-warn">Are you sure you want to add the item ?</div>',
        width      : 410,
        zIndex     : 6,
        centered   : true,
        modal      : false, // modal behavior
        render     : '.example',
        visible    : false, // make visible explicitly with .show()
        buttons    : {
            footer: [
                {
                    name  : 'cancel',
                    label : 'Cancel',
                    action: 'onCancel'
                },

                {
                    name     : 'proceed',
                    label    : 'OK',
                    action   : 'onOK'
                }
            ]
        }
    });

    confirmDialog.onCancel = function (e) {
        e.preventDefault();
        this.hide();
        // the callback is not executed, and is
        // callback reference removed, so it won't persist
        this.callback = false;
    };

    confirmDialog.onOK = function (e) {
        e.preventDefault();
        this.hide();
        // code that executes the user confirmed action goes here
        if(this.callback){
            this.callback();
        }
        // callback reference removed, so it won't persist
        this.callback = false;
    };

    /**
     * Constructor for the AddInventoryBinderIndex class.
     *
     * @class AddInventoryBinderIndex
     * @constructor
     */
    Y.namespace('mojito.binders')[NAME] = {

        /**
         * Binder initialization method, invoked after all binders on the page
         * have been constructed.
         */
        init: function(mojitProxy) {
            this.mojitProxy = mojitProxy;
        },

        /**
         * The binder method, invoked to allow the mojit to attach DOM event
         * handlers.
         *
         * @param node {Node} The DOM node to which this mojit is attached.
         */
        bind: function(node) {
            var me = this;
            this.node = node;
            this.bindAddButton(node);
            this.bindCancelButton(node);
            this.bindEnableAddButton(node);
        },

        bindEnableAddButton: function(node){

            function valChange() {
                var typeP = Y.one('#typeP').get('value'),
                    nameP = Y.one('#nameP').get('value');
                if (typeP.length >= 4 && nameP.length >= 2) {
                    Y.one('#addButton').setStyle('background-color', ' #E9DC51');
                    Y.one('#addButton').set('disabled', false);
                }
                else {
                    Y.one('#addButton').setStyle('background-color', ' #999999');
                    Y.one('#addButton').set('disabled', true);
                }
            }
            Y.one('#typeP').on("blur", valChange);
            Y.one('#nameP').on("blur", valChange);
        },

        bindAddButton: function(node) {

            function btnClick()
            {
                var typeP = Y.one('#typeP').get('value'),
                    nameP = Y.one('#nameP').get('value'),
                    descP = Y.one('#descP').get('value');

                var addConfirm = function() {
                    Y.io("/inventoryList/inventory", {
                        method: "POST",
                        headers: {
                            "Content-Type": "application/json"
                        },
                        data: JSON.stringify({typeP: typeP, nameP: nameP, descP: descP}),
                        on: {
                            complete: function (id, o, args) {

                                var response = JSON.parse(o.responseText),
                                    response1 = JSON.parse(response.body);
                                if (response1.status.message === "success" && response1.inventory) {
                                    Y.one('#success-div').setStyle('display', 'block');
                                    Y.config.win.location = '/dashboard';
                                } else {
                                    Y.one('#error-div').setStyle('display', 'block');
                                }

                            }
                        }
                    });
                };
                confirmDialog.callback = addConfirm;
                confirmDialog.show();
            }
            Y.one('#addButton').on("click", btnClick);
        },

        bindCancelButton: function(node) {

            function btnClick() {
                Y.config.win.location='/dashboard';
            }
            Y.one('#cancelButton').on("click", btnClick);
        }
    };

}, '0.0.1', {requires: ['event-mouseenter', 'mojito-client', 'event-click', 'panel']});
