/*jslint anon:true, sloppy:true, nomen:true*/
/**
 * Copyright 2013 GwynnieBee Inc.
 */


/*global YUI,$,Panel, alert*/
YUI.add('employeesinventory-binder-index', function(Y, NAME) {

/**
 * The employeesinventory-binder-index module.
 *
 * @module employeesinventory-binder-index
 */

    var panel = new Y.Panel({
        srcNode: '#inventoryField',
        headerContent: 'Inventory Detail',
        width: 500,
        zIndex: 5,
        centered: true,
        modal: true,
        visible: false,
        render: true
    });

    var dialog = new Y.Panel({
        contentBox : Y.Node.create('<div id="dialog" />'),
        bodyContent: '<div class="message icon-warn">Are you sure ?</div>',
        width      : 410,
        zIndex     : 6,
        centered   : true,
        modal      : false, // modal behavior
        render     : '.example',
        visible    : false, // make visible explicitly with .show()
        buttons    : {
            footer: [
                {
                    name  : 'cancel',
                    label : 'Cancel',
                    action: 'onCancel'
                },

                {
                    name     : 'proceed',
                    label    : 'OK',
                    action   : 'onOK'
                }
            ]
        }
    });

    dialog.onCancel = function (e) {
        e.preventDefault();
        this.hide();
        // the callback is not executed, and is
        // callback reference removed, so it won't persist
        this.callback = false;
    };

    dialog.onOK = function (e) {
        e.preventDefault();
        this.hide();
        // code that executes the user confirmed action goes here
        if(this.callback){
            this.callback();
        }
        // callback reference removed, so it won't persist
        this.callback = false;
    };

    /**
     * Constructor for the EmployeesInventoryBinderIndex class.
     *
     * @class EmployeesInventoryBinderIndex
     * @constructor
     */
    Y.namespace('mojito.binders')[NAME] = {

        /**
         * Binder initialization method, invoked after all binders on the page
         * have been constructed.
         */
        init: function(mojitProxy) {
            this.mojitProxy = mojitProxy;
        },

        /**
         * The binder method, invoked to allow the mojit to attach DOM event
         * handlers.
         *
         * @param node {Node} The DOM node to which this mojit is attached.
         */
        bind: function(node) {
            var me = this;
            this.node = node;
            this.bindEmployeeInventorySearchButton(node);
            this.bindPopupInventoryDetails(node);
            this.bindReturnItemButton(node);
            this.bindCancelButton(node);
        },

        bindCancelButton: function(node) {
            function btnClick(e) {
                e.preventDefault();
                Y.one('#popup-div').setStyle('display', "none");
                //panel.hide();
            }
            Y.one('#cancelButton').on("click", btnClick);
        },

        bindReturnItemButton: function(node) {
            function btnClick(e) {
                e.preventDefault();
                var job = "return",
                    returnedTo = "adminX",
                    itemId = Y.one('#itemId').get('value'),
                    rowId = '#row_'+itemId,
                    row = Y.one(rowId);

                var yes = function() {
                    Y.io("/inventoryList/inventoryDetail/job", {
                        method: "POST",
                        headers: {
                            "Content-Type": "application/json"
                        },
                        data: JSON.stringify({job: job, itemId: itemId, returnedTo: returnedTo}),
                        on: {
                            complete: function (id, o, args) {
                                var response = JSON.parse(o.responseText),
                                    response1 = JSON.parse(response.body);
                                if (o.responseText) {
                                    if (response1.status.message === "success") {
                                        row.ancestor().removeChild(row);
                                        Y.one('#popup-div').setStyle('display', "none");
                                    } else {
                                        Y.one('#popup-div').setStyle('display', "none");
                                    }
                                }
                            }
                        }
                    });
                };
                dialog.callback = yes;
                dialog.show();
            }
            Y.one('#returnItemButton').on("click", btnClick);
        },

        bindPopupInventoryDetails: function (node) {
            function btnClick(e) {
                e.preventDefault();
                var itemId = e.currentTarget.getData('itemId');
                //alert("item id is = " + itemId);
                Y.one('#assignButton').set('disabled', true);
                Y.one('#modifyButton').set('disabled', true);
                Y.one('#deleteItemButton').set('disabled', true);
                //Y.one('#assignButton').setStyle('background-color', '#999999');
                //Y.one('#modifyButton').setStyle('background-color', '#999999');
                //Y.one('#deleteButton').setStyle('background-color', '#999999');
                Y.io("/inventoryList/inventory/" + itemId, {
                    method: "GET",
                    headers: {
                        "Content-Type": "application/json"
                    },
                    on: {
                        complete: function (id, o, args) {
                            if (o.responseText) {
                                var item = JSON.parse(o.responseText);
                                Y.one('#itemId').set('value', item.itemDetail.itemId);
                                Y.one('#itemType').set('value', item.itemDetail.itemType);
                                Y.one('#itemName').set('value', item.itemDetail.itemName);
                                Y.one('#itemDesc').set('value', item.itemDetail.description);
                                Y.one('#status').set('value', item.itemDetail.status);
                                Y.one('#issuedTo').set('value', item.itemDetail.issuedTo);
                                Y.one('#issuedOn').set('value', item.itemDetail.issuedOn);
                                Y.one('#expectedReturnDate').set('value', item.itemDetail.expectedReturn);
                                Y.one('#lastIssuedTo').set('value', item.itemDetail.lastIssuedTo);
                                Y.one('#lastReturnedOn').set('value', item.itemDetail.returnedOn);
                                Y.one('#lastReturnedTo').set('value', item.itemDetail.returnedTo);
                                Y.one('#createdOn').set('value', item.itemDetail.createdOn);

                                Y.one('#assignButton').setStyle('background-color', '#999999');
                                Y.one('#modifyButton').setStyle('background-color', '#999999');
                                Y.one('#deleteItemButton').setStyle('background-color', '#999999');

                                Y.one('#popup-div').setStyle('display', "inline");
                                panel.show();
                                $('#popup-div div').eq(1).css({left:'400px', top:'100px'});
                            }
                        }
                    }
                });

            }
            Y.one('#invList').delegate("click", btnClick, 'a');
        },

        bindEmployeeInventorySearchButton: function(node) {
            function btnClick(e)
            {
                if (e.which === 13 || e.type === "click") {
                    var employeeId = Y.one('#employeeItemSearch').get('value');

                    Y.io("/employeeInventoryList", {
                        method: "POST",
                        headers: {
                            "Content-Type": "application/json"
                        },
                        data: JSON.stringify({employeeId: employeeId}),
                        on: {
                            complete: function (id, o, args)
                            {
                                if(o.responseText) {
                                    Y.one('#empty').setHTML(o.responseText);
                                    Y.one('#invList').setHTML(Y.one('#empty #invList').getHTML());
                                    Y.one('#empty').setHTML('');
                                }
                            }
                        }
                    });
                }
            }
            Y.one('#searchButton').on("click", btnClick);
            $('#employeeItemSearch').on("keypress", btnClick);
            $("#searchEmp").on("submit", function(e) {e.preventDefault();});
        }
    };

}, '0.0.1', {requires: ['event-mouseenter', 'mojito-client', 'panel']});
