/*jslint anon:true, sloppy:true, nomen:true*/
/**
 * Copyright 2013 GwynnieBee Inc.
 */


/*global YUI*/
YUI.add('employeesinventory', function(Y, NAME) {

/**
 * The employeesinventory module.
 *
 * @module employeesinventory
 */

    /**
     * Constructor for the Controller class.
     *
     * @class Controller
     * @constructor
     */
    Y.namespace('mojito.controllers')[NAME] = {

        /**
         * Method corresponding to the 'index' action.
         *
         * @param ac {Object} The ActionContext that provides access
         *        to the Mojito API.
         */
        index: function(ac) {
            var employeeId = ac.params.getFromBody('employeeId');
            ac.models.get('model').getEmployeeInventoryList(employeeId, function(err, response) {
                if (err) {
                    ac.error(err);
                    return;
                }
                //ac.assets.addCss('./elements.css');
                if(response) {
                    var paramsArray = [],
                        inv = JSON.parse(response._resp.responseText);
                    //console.log(inv);
                    for (var i = 0; i < inv.inventorys.length; i++) {
                        paramsArray.push({"item": inv.inventorys[i]});
                    }
                    ac.done(
                        {
                            issuedTo: employeeId,
                            itemDetailsArray: paramsArray
                        }, 'index'
                    );
                }
                else{
                    ac.done({},"");
                }
            });
        }

    };

}, '0.0.1', {requires: ['mojito', 'mojito-assets-addon', 'mojito-models-addon', 'mojito-params-addon']});
