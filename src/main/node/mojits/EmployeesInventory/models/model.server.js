/*jslint anon:true, sloppy:true, nomen:true*/
/**
 * Copyright 2013 GwynnieBee Inc.
 */


/*global YUI*/
YUI.add('employeesinventory-model', function(Y, NAME) {

/**
 * The employeesinventory-model module.
 *
 * @module employeesinventory
 */

    /**
     * Constructor for the EmployeesInventoryModel class.
     *
     * @class EmployeesInventoryModel
     * @constructor
     */

    var CONFIG = require("config"),
        url = CONFIG.endpoints.employee;

    Y.namespace('mojito.models')[NAME] = {

        init: function(config) {
            this.config = config;
        },

        getData: function(callback) {
            callback(null, { some: 'data' });
        },

        getEmployeeInventoryList: function(employeeId, callback) {
            console.log("url = " + url + employeeId + ".json");
            Y.mojito.lib.REST.GET(url + employeeId + ".json",{}, {
                    headers: {"Content-Type": "application/json"},
                    timeout: 12000
                },
                function (err, response) {
                    console.log("getemployeInventoryList",response);
                    if(response) {
                        callback(null, response);
                    }
                    else{
                        callback(null,null);
                    }
                });
        }

    };

}, '0.0.1', {requires: ['config']});
