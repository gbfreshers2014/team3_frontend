/*jslint anon:true, sloppy:true, nomen:true*/
/**
 * Copyright 2013 GwynnieBee Inc.
 */


/*global YUI*/

YUI.add('inventorylist', function(Y, NAME) {

/**
 * The inventorylist module.
 *
 * @module inventorylist
 */

    /**
     * Constructor for the Controller class.
     *
     * @class Controller
     * @constructor
     */
    Y.namespace('mojito.controllers')[NAME] = {

        /**
         * Method corresponding to the 'index' action.
         *
         * @param ac {Object} The ActionContext that provides access
         *        to the Mojito API.
         */
        index: function(ac) {
            ac.models.get('model').getInventoryList(function(err, response) {
                if (err) {
                    ac.error(err);
                    return;
                }
                else {
                    if (response) {
                        var paramsArray = [],
                            inv = JSON.parse(response._resp.responseText);
                        for (var i = 0; i < inv.inventorys.length; i++) {
                            if (inv.inventorys[i].status === 1) {
                                inv.inventorys[i].status = "Available";
                            }
                            else if (inv.inventorys[i].status === 2) {
                                inv.inventorys[i].status = "Not Available";
                            }
                            paramsArray.push({"employee": inv.inventorys[i]});
                        }
                        ac.done(
                            {
                                params: paramsArray
                            }, "index"
                        );
                    }
                    else{
                        ac.done({},"");
                    }
                }
            });
        },

        availabilityFilter: function(ac) {
            ac.models.get('model').getFilterInventory(ac, function(err, response) {
                if (err) {
                    ac.error(err);
                    return;
                }
                else {
                    if(response) {
                        var paramsArray = [],
                            inv = JSON.parse(response._resp.responseText);
                        for (var i = 0; i < inv.inventorys.length; i++) {
                            if (inv.inventorys[i].status === 1) {
                                inv.inventorys[i].status = "Available";
                            }
                            else if (inv.inventorys[i].status === 2) {
                                inv.inventorys[i].status = "Not Available";
                            }
                            paramsArray.push({"employee": inv.inventorys[i]});
                        }
                        ac.done(
                            {
                                params: paramsArray
                            }, 'inventoryListFilter'
                        );
                    } else{
                        ac.done({},"");
                    }
                }
            });
        },

        inventoryDetailsPopup: function(ac) {
            var id = ac.params.getFromRoute('id');
            ac.models.get('model').getInventoryDetails(id, function(err, response) {
                if (err) {
                    ac.error(err);
                    return;
                }
                //ac.assets.addCss('./elements.css');
                var itemDetail = JSON.parse(response._resp.responseText);
                ac.done(
                    {
                        itemDetail: itemDetail.inventory
                    },'json'
                );
            });
        },

        assignItem: function(ac) {
            var user = ac._adapter.req.user.emailId;
            ac.models.get('model').assignItem(ac, user, function(err, response) {
                if (err) {
                    ac.error(err);
                    return;
                }
                else {
                    ac.done({body: response.res._resp.responseText}, "json");
                }
            });
        },

        employeeValidation: function(ac) {
            var employee = ac.params.getFromBody('issuedTo');
            ac.models.get('model').validateEmployee(ac, employee, function(err, response) {
                if (err) {
                    ac.error(err);
                    return;
                }
                else {
                     ac.done(response._resp.responseText, "json");
                }

            });
        }
    };

}, '0.0.1', {requires: ['mojito', 'mojito-assets-addon', 'mojito-models-addon', 'mojito-params-addon']});