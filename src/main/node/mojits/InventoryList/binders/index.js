/*jslint anon:true, sloppy:true, nomen:true*/
/**
 * Copyright 2013 GwynnieBee Inc.
 */


/*global YUI, alert, $*/
YUI.add('inventorylist-binder-index', function(Y, NAME) {
/**
 * The inventorylist-binder-index module.
 *
 * @module inventorylist-binder-index
 */


    var pageNo = 0,
        filterStatus= "",
        panel = new Y.Panel({
        srcNode: '#inventoryField',
        headerContent: 'Inventory Detail',
        width: 500,
        zIndex: 5,
        centered: true,
        modal: true,
        visible: false,
        render: true,
        plugins: [Y.Plugin.Drag]
    });

    var confirmDialog = new Y.Panel({
        contentBox : Y.Node.create('<div id="dialog" />'),
        bodyContent: '<div class="message icon-warn">Are you sure ?</div>',
        width      : 410,
        zIndex     : 6,
        centered   : true,
        modal      : false, // modal behavior
        render     : '.example',
        visible    : false, // make visible explicitly with .show()
        buttons    : {
            footer: [
                {
                    name  : 'cancel',
                    label : 'Cancel',
                    action: 'onCancel'
                },

                {
                    name     : 'proceed',
                    label    : 'OK',
                    action   : 'onOK'
                }
            ]
        }
    });

    var errorDialog = new Y.Panel({
        contentBox : Y.Node.create('<div id="dialog" />'),
        bodyContent: '<div class="errorMessage icon-warn">Sorry could not complete the action..!!</div>',
        width      : 410,
        zIndex     : 6,
        centered   : true,
        modal      : false, // modal behavior
        render     : '.example',
        visible    : false, // make visible explicitly with .show()
        buttons    : {
            footer: [
                {
                    name     : 'proceed',
                    label    : 'OK',
                    action   : 'onOK'
                }
            ]
        }
    });

    errorDialog.onOK = function (e) {
        e.preventDefault();
        this.hide();
        // the callback is not executed, and is
        // callback reference removed, so it won't persist
        this.callback = false;
    };

    confirmDialog.onCancel = function (e) {
        e.preventDefault();
        this.hide();
        // the callback is not executed, and is
        // callback reference removed, so it won't persist
        this.callback = false;
    };

    confirmDialog.onOK = function (e) {
        e.preventDefault();
        this.hide();
        // code that executes the user confirmed action goes here
        if(this.callback){
            this.callback();
        }
        // callback reference removed, so it won't persist
        this.callback = false;
    };
    /**
     * Constructor for the InventoryListBinderIndex class.
     *
     * @class InventoryListBinderIndex
     * @constructor
     */
     Y.namespace('mojito.binders')[NAME] = {

         init: function (mojitProxy) {
             this.mojitProxy = mojitProxy;
         },

         bind: function (node) {
             var me = this;
             this.node = node;
             this.bindAvailableFilter(node);
             this.bindPopupInventoryDetails(node);
             this.bindAssignButton(node);
             this.bindReturnItemButton(node);
             this.bindDeleteItemButton(node);
             this.bindCancelButton(node);
             this.bindModifyButton(node);
         },

         bindModifyButton: function(node) {
             function btnClick(e) {
                 e.preventDefault();
                 var job = "modify",
                 itemId = Y.one('#itemId').get('value'),
                 itemType = Y.one('#itemType').get('value'),
                 updatedBy = "adminX",
                 itemName = Y.one('#itemName').get('value'),
                 description = Y.one('#itemDesc').get('value'),
                 rowId = '#row_'+itemId;

                 var yes = function() {
                     Y.io("/inventoryList/inventoryDetail/job", {
                         method: "POST",
                         headers: {
                             "Content-Type": "application/json"
                         },
                         data: JSON.stringify({job: job, itemId: itemId, updatedBy: updatedBy, itemType: itemType, itemName: itemName, description: description}),
                         on: {
                             complete: function (id, o, args) {
                                 var response = JSON.parse(o.responseText),
                                     response1 = JSON.parse(response.body);
                                 if (o.responseText) {
                                     if (response1.status.message === "success") {
                                         Y.one(rowId + ' .type').set('text', itemType);
                                         Y.one(rowId + ' .name').set('text', itemName);
                                         Y.one('#popup-div').setStyle('display', "none");
                                         var bcolor = Y.one(rowId).getStyle('background-color');
                                         Y.one(rowId).setStyle('background-color', 'orange');
                                         setTimeout(function () {
                                             Y.one(rowId).setStyle('background-color', bcolor);
                                         }, 1000);
                                     } else {
                                         Y.one('#dialog .errorMessage').setHTML('Some error occurred. Please try again');
                                         errorDialog.show();
                                         Y.one('#popup-div').setStyle('display', "none");
                                     }
                                 }
                             }
                         }
                     });
                 };
                 confirmDialog.callback = yes;
                 confirmDialog.show();
             }
             Y.one('#modifyButton').on("click", btnClick);
         },

         bindCancelButton: function(node) {
             function btnClick(e) {
                 e.preventDefault();
                 Y.one('#popup-div').setStyle('display', "none");
                 //panel.hide();
             }
             Y.one('#cancelButton').on("click", btnClick);
         },

         bindDeleteItemButton: function(node) {
             function btnClick(e) {
                 e.preventDefault();
                 var updatedBy = "adminX",
                     itemId = Y.one('#itemId').get('value'),
                     rowId = '#row_'+itemId,
                     row = Y.one(rowId);

                 var yes = function() {
                     Y.io("/inventoryList/inventoryDetail/job", {
                         method: "POST",
                         headers: {
                             "Content-Type": "application/json"
                         },
                         data: JSON.stringify({itemId: itemId, updatedBy: updatedBy}),
                         on: {
                             complete: function (id, o, args) {
                                 var response = JSON.parse(o.responseText),
                                     response1 = JSON.parse(response.body);
                                 if (o.responseText) {
                                     if (response1.message === "success") {
                                         //alert("Item deleted from Inventory..!!");
                                         row.ancestor().removeChild(row);
                                         Y.one('#popup-div').setStyle('display', "none");
                                     } else {
                                         Y.one('#dialog .errorMessage').setHTML('Item can not be deleted...!!!');
                                         errorDialog.show();
                                         Y.one('#popup-div').setStyle('display', "none");
                                         //$('#popup-div div').eq(1).css({left:'0px', top:'0px'});
                                     }
                                 }
                             }
                         }
                     });
                 };
                 confirmDialog.callback = yes;
                 confirmDialog.show();
             }
             Y.one('#deleteItemButton').on("click", btnClick);
         },

         bindReturnItemButton: function(node) {
             function btnClick(e) {
                 e.preventDefault();
                 var job = "return",
                     returnedTo = "adminX",
                     itemId = Y.one('#itemId').get('value'),
                     cellId = '#row_'+itemId + ' .status',
                     cell = Y.one(cellId);

                 var yes = function() {
                     Y.io("/inventoryList/inventoryDetail/job", {
                         method: "POST",
                         headers: {
                             "Content-Type": "application/json"
                         },
                         data: JSON.stringify({job: job, itemId: itemId, returnedTo: returnedTo}),
                         on: {
                             complete: function (id, o, args) {
                                 var response = JSON.parse(o.responseText),
                                     response1 = JSON.parse(response.body);
                                 if (o.responseText) {
                                     if (response1.status.message === "success") {
                                         cell.set('text', 'Available');
                                         Y.one('#popup-div').setStyle('display', "none");
                                         var bcolor = cell.getStyle('background-color');
                                         //$('#popup-div div').eq(1).css({left:'0px', top:'0px'});
                                         cell.setStyle('background-color', 'orange');
                                         setTimeout(function () {
                                             cell.setStyle('background-color', bcolor);
                                         }, 1000);
                                     } else {
                                         Y.one('#dialog .errorMessage').setHTML('Some error occurred. Please try again.');
                                         errorDialog.show();
                                         Y.one('#popup-div').setStyle('display', "none");
                                         //$('#popup-div div').eq(1).css({left:'0px', top:'0px'});
                                     }
                                 }
                             }
                         }
                     });
                 };
                 confirmDialog.callback = yes;
                 confirmDialog.show();
             }
             Y.one('#returnItemButton').on("click", btnClick);
         },

         bindAssignButton: function(node) {
             function btnClick(e) {
                 e.preventDefault();
                 var job = "assign",
                     itemId = Y.one('#itemId').get('value'),
                     issuedTo = Y.one('#issuedTo').get('value'),
                     issuedBy = "adminX",
                     expectedReturn = Y.one('#expectedReturnDate').get('value'),
                     cellId = '#row_'+itemId + ' .status',
                     cell = Y.one(cellId);
                 if (!issuedTo) {
                     Y.one('#issuedTo').setStyle('border-color', 'red');
                 }
                 else {
                     Y.io("/inventoryList/inventoryDetail/job/validation", {
                         method: "POST",
                         headers: {
                             "Content-Type": "application/json"
                         },
                         data: JSON.stringify({issuedTo: issuedTo}),
                         on: {
                             complete: function (id, o, args) {
                                 var response = JSON.parse(JSON.parse(o.responseText));
                                 if (o.responseText) {
                                     if (response.code === 1) {
                                         confirmDialog.callback = yes;
                                         confirmDialog.show();
                                     } else {
                                         //alert("Not valid user....!!!");
                                         Y.one('#dialog .errorMessage').setHTML('Not valid employee..!!');
                                         errorDialog.show();
                                     }
                                 }
                             }
                         }
                     });
                 }


                     var yes = function() {
                     Y.io("/inventoryList/inventoryDetail/job", {
                         method: "POST",
                         headers: {
                             "Content-Type": "application/json"
                         },
                         data: JSON.stringify({job: job, itemId: itemId, issuedTo: issuedTo, issuedBy: issuedBy, expectedReturn: expectedReturn}),
                         on: {
                             complete: function (id, o, args) {
                                 var response = JSON.parse(o.responseText),
                                     response1 = JSON.parse(response.body);
                                 if (o.responseText) {
                                     if (response1.status.message === "success") {
                                         cell.set('text', 'Not Available');
                                         Y.one('#popup-div').setStyle('display', "none");
                                         var bcolor = cell.getStyle('background-color');
                                         cell.setStyle('background-color', 'orange');
                                         setTimeout(function () {
                                             cell.setStyle('background-color', bcolor);
                                         }, 1000);
                                     } else {
                                         errorDialog.show();
                                         Y.config.win.location = '/inventoryList';
                                     }
                                 }
                             }
                         }
                     });
                 };

             }
             Y.one('#assignButton').on("click", btnClick);
         },

         bindPopupInventoryDetails: function (node) {
             function btnClick(e) {
                 e.preventDefault();
                 var itemId = e.currentTarget.getData('itemId');

                 Y.io("/inventoryList/inventory/" + itemId, {
                     method: "GET",
                     headers: {
                         "Content-Type": "application/json"
                     },
                     on: {
                         complete: function (id, o, args) {
                             if (o.responseText) {
                                 var item = JSON.parse(o.responseText);
                                 Y.one('#itemId').set('value', item.itemDetail.itemId);
                                 Y.one('#itemType').set('value', item.itemDetail.itemType);
                                 Y.one('#itemName').set('value', item.itemDetail.itemName);
                                 Y.one('#itemDesc').set('value', item.itemDetail.description);
                                 Y.one('#status').set('value', item.itemDetail.status);
                                 Y.one('#issuedTo').set('value', item.itemDetail.issuedTo);
                                 Y.one('#issuedOn').set('value', item.itemDetail.issuedOn);
                                 Y.one('#expectedReturnDate').set('value', item.itemDetail.expectedReturn);
                                 Y.one('#lastIssuedTo').set('value', item.itemDetail.lastIssuedTo);
                                 Y.one('#lastReturnedOn').set('value', item.itemDetail.returnedOn);
                                 Y.one('#lastReturnedTo').set('value', item.itemDetail.returnedTo);
                                 Y.one('#createdOn').set('value', item.itemDetail.createdOn);
                                 if(item.itemDetail.status === 2) {
                                     //Y.one('#issuedTo').setAttribute("readonly", 'readonly');
                                     Y.one('#assignButton').setStyle('background-color', ' #999999');
                                     Y.one('#assignButton').set('disabled', true);
                                     Y.one('#modifyButton').setStyle('background-color', ' #999999');
                                     Y.one('#modifyButton').set('disabled', true);
                                     Y.one('#deleteItemButton').setStyle('background-color', ' #999999');
                                     Y.one('#deleteItemButton').set('disabled', true);
                                     Y.one('#returnItemButton').setStyle('background-color', ' #E9DC51');
                                     Y.one('#returnItemButton').set('disabled', false);
                                 }
                                 if(item.itemDetail.status === 1) {
                                     Y.one('#returnItemButton').setStyle('background-color', ' #999999');
                                     Y.one('#returnItemButton').set('disabled', true);
                                     //Y.one('#issuedTo').setAttribute("readonly", false);
                                     Y.one('#assignButton').setStyle('background-color', '#E9DC51');
                                     Y.one('#assignButton').set('disabled', false);
                                     Y.one('#modifyButton').setStyle('background-color', '#E9DC51');
                                     Y.one('#modifyButton').set('disabled', false);
                                     Y.one('#deleteItemButton').setStyle('background-color', '#E9DC51');
                                     Y.one('#deleteItemButton').set('disabled', false);
                                 }
                                 Y.one('#popup-div').setStyle('display', "inline");
                                 panel.show();
                                 $('#popup-div div').eq(1).css({left:'400px', top:'100px'});
                             }
                         }
                     }
                 });
             }
             Y.one('#invList').delegate("click", btnClick, 'a');
         },

         bindAvailableFilter: function(node) {
             function btnClick(e) {
                 pageNo = 0;
                 if (e.currentTarget.get('id') !== 'searchButton') {
                     filterStatus = e.currentTarget.get('value');
                 }
                 var itemId = Y.one('#itemIdSearch').get('value'),
                     itemName = Y.one('#itemNameSearch').get('value'),
                     itemType = Y.one('#itemTypeSearch').get('value'),
                     status = 0;

                 if (filterStatus === "available") {
                     status = 1;
                 } else if (filterStatus === "notAvailable") {
                     status = 2;
                 }
                 else {
                     status = 0;
                 }
                 Y.io("/inventoryList", {
                     method: "POST",
                     headers: {
                         "Content-Type": "application/json"
                     },
                     data: JSON.stringify({status: status, page: pageNo, itemId: itemId, itemName: itemName, itemType: itemType}),
                     on: {
                         complete: function (id, o, args) {

                             if (o.responseText) {

                                 Y.one('#empty').setHTML(o.responseText);
                                 Y.one('#invList').setHTML(Y.one('#empty #invList').getHTML());
                                 Y.one('#empty').setHTML('');
                             }
                         }
                     }
                 });
             }

             function btnClick1(e) {
                 //var filterStatus = Y.one('#availabilityFilter input[name=filter]:checked').get('value'),
                 // var event=e.currentTarget;
                 e.preventDefault();
                 if (e.currentTarget.get('id') === 'previous') {
                     if (pageNo !== 0) {
                         pageNo = pageNo - 1;
                     }
                 }
                 else if (e.currentTarget.get('id') === 'next') {
                     pageNo = pageNo + 1;

                 }
                 //var filterStatus = e.currentTarget.get('value'),
                 var itemId = Y.one('#itemIdSearch').get('value'),
                     itemName = Y.one('#itemNameSearch').get('value'),
                     itemType = Y.one('#itemTypeSearch').get('value'),
                     status = 0;

                 if (filterStatus === "available") {
                     status = 1;
                 } else if (filterStatus === "notAvailable") {
                     status = 2;
                 }
                 else {
                     status = 0;
                 }
                 //alert("status = " + status);


                 Y.io("/inventoryList", {
                     method: "POST",
                     headers: {
                         "Content-Type": "application/json"
                     },
                     data: JSON.stringify({status: status, page: pageNo, itemId: itemId, itemName: itemName, itemType: itemType}),
                     on: {
                         complete: function (id, o, args) {

                             if (o.responseText) {

                                 Y.one('#empty').setHTML(o.responseText);
                                 Y.one('#invList').setHTML(Y.one('#empty #invList').getHTML());
                                 Y.one('#empty').setHTML('');
                             }
                         }
                     }
                 });
             }
             Y.all('input[type=radio]').on('change', btnClick);
             Y.one('#searchButton').on("click", btnClick);
             Y.one('#previous').on("click", btnClick1);
             Y.one('#next').on("click", btnClick1);
         }

     };

}, '0.0.1', {requires: ['event-mouseenter', 'mojito-client', 'panel', 'popup-binder-index']});
