/*jslint anon:true, sloppy:true, nomen:true*/
/**
 * Copyright 2013 GwynnieBee Inc.
 */


/*global YUI*/

YUI.add('inventorylist-model', function(Y, NAME) {

/**
 * The inventorylist-model module.
 *
 * @module inventorylist
 */

    /**
     * Constructor for the InventoryListModel class.
     *
     * @class InventoryListModel
     * @constructor
     */

    //var request = require("request"),
      var limit = 20, offset = 0,
        CONFIG = require("config"),
        url = CONFIG.endpoints.list + '?limit='+limit+'&offset=';


    Y.namespace('mojito.models')[NAME] = {

        init: function(config) {
            this.config = config;
        },

        //ac.params.getFromBody("status");
        getInventoryList: function(callback) {
            Y.mojito.lib.REST.GET(url+offset,{}, {
                    headers: {"Content-Type": "application/json"},
                    timeout: 12000
                },
                function (err, response) {
                   if(response) {
                       callback(null, response);
                   }
                    else{
                       callback(null,null);
                   }
                });
        },

        getFilterInventory: function(ac, callback) {
            var status = ac.params.getFromBody("status"),
                itemId = ac.params.getFromBody("itemId"),
                itemName = ac.params.getFromBody("itemName"),
                itemType = ac.params.getFromBody("itemType"),
                pageNo = ac.params.getFromBody("page");
            var and = '&',
                statusQuery = 'status="',
                itemIdQuery = 'itemId="',
                itemNameQuery = 'itemName="',
                itemTypeQuery = 'itemType="',
                query = '';

            offset = pageNo * 20;

            if( status !== 0 ) {
                query = statusQuery + status + '"';
            }
            if( itemId ) {
                if( query ) {
                    query += and + itemIdQuery + itemId + '"';
                }
                else {
                    query += itemIdQuery + itemId + '"';
                }
            }
            if( itemName ) {
                if( query ) {
                    query += and + itemNameQuery + itemName + '"';
                }
                else {
                    query += itemNameQuery + itemName + '"';
                }
            }
            if( itemType ) {
                if( query ) {
                    query += and + itemTypeQuery + itemType + '"';
                }
                else {
                    query += itemTypeQuery + itemType + '"';
                }
            }
            if( status === 0 &&  itemId === null && itemName === null && itemType === null ) {
                Y.mojito.lib.REST.GET(url + offset,{}, {
                        headers: {"Content-Type": "application/json"},
                        timeout: 12000
                    },
                    function (err, response) {
                        if(response) {
                            callback(null, response);
                        }
                        else{
                            callback(null,null);
                        }
                    }
                );
            }
            else {
                Y.mojito.lib.REST.GET(url + offset + '&' + query, {}, {
                        headers: {"Content-Type": "application/json"},
                        timeout: 12000
                    },
                    function (err, response) {
                        if(response) {
                            callback(null, response);
                        }
                        else{
                            callback(null,null);
                        }
                    }
                );
            }
        },

        getInventoryDetails: function(itemId, callback){
            var url2 = CONFIG.endpoints.list2+itemId+".json";
            Y.mojito.lib.REST.GET(url2,{}, {
                    headers: {"Content-Type": "application/json"},
                    timeout: 12000
                },
                function (err, response) {
                    if(response) {
                        callback(null, response);
                    }
                    else {
                        callback(null,null);
                    }
                }
            );
        },

        assignItem: function(ac, user, callback) {
            var job = ac.params.getFromBody("job"),
                itemId = ac.params.getFromBody("itemId"),
                url2 = CONFIG.endpoints.list2 + itemId + ".json",
                updatedBy = user;

            if ( job && job === "assign" ) {
                var issuedTo = ac.params.getFromBody("issuedTo"),
                    //issuedBy = ac.params.getFromBody("issuedBy"),
                    issuedBy = user,
                    expectedReturn = ac.params.getFromBody("expectedReturn");

                Y.mojito.lib.REST.PUT(url2, JSON.stringify({
                    "issuedBy": issuedBy,
                    "issuedTo": issuedTo,
                    "expectedReturn": expectedReturn,
                    "job": job
                }), {
                    headers: {"Content-Type": "application/json"},
                    timeout: 12000
                }, function (err, response) {
                    if(response) {
                        callback(null, {res: response});
                    }
                    else {
                        callback(null,null);
                    }
                });
            } else if( job && job === "modify" ) {
                var itemType = ac.params.getFromBody("itemType"),
                    itemName = ac.params.getFromBody("itemName"),
                    description = ac.params.getFromBody("description");

                Y.mojito.lib.REST.PUT(url2, JSON.stringify({
                    "itemType": itemType,
                    "itemName": itemName,
                    "updatedBy": updatedBy,
                    "description" : description,
                    "job": job
                }), {
                    headers: {"Content-Type": "application/json"},
                    timeout: 12000
                }, function (err, response) {
                    console.log(err, '--err');
                    console.log(response, '--response');
                    if(response) {
                        callback(null, {res: response});
                    }
                    else {
                        callback(null,null);
                    }
                });

            } else if( job && job === "return" ) {
                //var returnedTo = ac.params.getFromBody("returnedTo");
                var returnedTo = user;

                Y.mojito.lib.REST.PUT(url2, JSON.stringify({
                    "returnedTo": returnedTo,
                    "job": job
                }), {
                    headers: {"Content-Type": "application/json"},
                    timeout: 12000
                }, function (err, response) {
                   if(response) {
                        callback(null, {res: response});
                    }
                    else {
                        callback(null,null);
                    }
                });

            }
            else {

                Y.mojito.lib.REST.DELETE(url2, JSON.stringify({
                    "updatedBy": updatedBy
                }), {
                    headers: {"Content-Type": "application/json"},
                    timeout: 12000
                }, function (err, response) {
                    if(response) {
                        callback(null, {res: response});
                    }
                    else {
                        callback(null,null);
                    }
                });

            }
        },

        validateEmployee: function(ac, employee, callback) {
            var validateURL = CONFIG.endpoints.validate+employee+".json";
            Y.mojito.lib.REST.GET(validateURL,{}, {
                    headers: {"Content-Type": "application/json"},
                    timeout: 12000
                },
                function (err, response) {
                    if(response) {
                        callback(null, response);
                    }
                    else {
                        callback(null,null);
                    }
                }
            );

        }
    };

}, '0.0.1', {requires: ['mojito-rest-lib', 'config']});
