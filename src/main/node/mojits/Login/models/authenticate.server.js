/*jslint anon:true, sloppy:true, nomen:true*/
/**
 * Copyright 2013 GwynnieBee Inc.
 */


/*global YUI*/
YUI.add('login-model-authenticate', function(Y, NAME) {
    var request = require("request");

    Y.namespace('mojito.models')[NAME] = {

        init: function(config) {
            this.config = config;
        },

        /**
         * Method that will be invoked by the mojit controller to obtain data.
         *
         * @param callback {function(err,data)} The callback function to call when the
         *        data has been retrieved.
         */
        getData: function(callback) {
            callback(null, { some: 'data' });
        },

        auth: function(un,ps,callback)
        {
            request({
                url: "http://192.168.1.126:8080/employee_information_management_t2-1.0.0-SNAPSHOT/authenticate/"+un+".json",
                //headers: {"Content-Type": "application/json"},
                method: "POST",
                body: {
                    "username" :un,
                    "password" :ps
                },
                json: true

            }, function(err,res, body){
                console.log(err || body);
                console.log(typeof body);
                callback(null,{msg: body.status.message});

            });
        }


    };

}, '0.0.1', {requires: []});
