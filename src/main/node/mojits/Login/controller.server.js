/*jslint anon:true, sloppy:true, nomen:true*/
/**
 * Copyright 2013 GwynnieBee Inc.
 */


/*global YUI*/
YUI.add('login', function(Y, NAME) {

/**
 * The login module.
 *
 * @module login
 */

    /**
     * Constructor for the Controller class.
     *
     * @class Controller
     * @constructor
     */
    Y.namespace('mojito.controllers')[NAME] = {
        index: function(ac) {
            var attempt = ac.params.getFromUrl('attempt');
            var msg = '';

            if(parseInt(attempt)===1){
                msg = "Invalid Login..!!";
            }
            else if(parseInt(attempt)===2){
                msg = "Only Admin is allowed to Login..!!";
            }
            ac.done({msg: msg},"login");
        },

        authenticate: function(ac) {
            var un = ac.params.getFromBody("userid");
            var ps = ac.params.getFromBody("pwd");


            ac.models.get('authenticate').auth(un,ps, function(err, response)
            {
                if(err)
                {
                    ac.done({msg:"Error"},"json");
                }

                else
                {
                    //ac.done({msg:response.authen},"json");
                    ac.done({msg:response.msg},"json");

                }

            });
        }
    };

}, '0.0.1', {requires: ['mojito', 'mojito-assets-addon', 'mojito-models-addon', 'dump', 'mojito-params-addon']});
