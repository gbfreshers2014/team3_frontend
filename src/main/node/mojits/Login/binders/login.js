/*jslint anon:true, sloppy:true, nomen:true*/
/**
 * Copyright 2013 GwynnieBee Inc.
 */


/*global YUI*/
YUI.add('login-binder-index', function(Y, NAME) {

/**
 * The login-binder-index module.
 *
 * @module login-binder-index
 */

    /**
     * Constructor for the LoginBinderIndex class.
     *
     * @class LoginBinderIndex
     * @constructor
     */
    Y.namespace('mojito.binders')[NAME] = {

        /**
         * Binder initialization method, invoked after all binders on the page
         * have been constructed.
         */
        init: function(mojitProxy) {
            this.mojitProxy = mojitProxy;
        },

        /**
         * The binder method, invoked to allow the mojit to attach DOM event
         * handlers.
         *
         * @param node {Node} The DOM node to which this mojit is attached.
         */
        bind: function(node) {
            var me = this;
            this.node = node;
           // this.bindloginButtons(node);
        }
        /*,

        bindloginButtons: function(node) {
            Y.one('#subButton').on("click", btnClick);
            function btnClick()
            {
                var userid = Y.one('#userid').get('value'),
                    pwd = Y.one('#pwd').get('value'),
                    re = /^([\w-]+(?:\.[\w-]+)*)@gwynniebee\.com$/;

    //            if( !re.test(userid) ) {
      //              alert("Not a valid e-mail address");
        //            return false;
          //      }


                Y.io("/login", {
                    method: "POST",
                    headers: {
                        "Content-Type": "application/json"
                    },
                    data: JSON.stringify({userid: userid, pwd: pwd}),
                    on: {
                        complete: function (id, o, args)
                        {
                            var response = JSON.parse(o.responseText);
                            //alert(o.responseText);
                            if(response.msg == "Success..:)")
                            {
                                Y.config.win.location='/dashboard';
                            }
                            else
                            {
                                alert('Could not login...!!');
                            }
                        }
                    }
                });

            }
        }*/
    };

}, '0.0.1', {requires: ['event-mouseenter', 'mojito-client', 'event-click']});
