#! /usr/bin/env python
#
# Does the following:
# - Runs mojito tests
# - Stops existing service if any
#

import os
import sys

mojito_app_dir = "/home/gb/share/mojito/team3-frontend"


def main():
    """Main function"""

    print "Running pre-install script..."

    # Stop service if running.
    if os.path.islink("/service/team3-frontend"):
        os.chdir("/service/team3-frontend")

        # Remove the symlink
        try:
            print "Unlinking /service/team3-frontend"
            os.unlink("/service/team3-frontend")
        except Exception as ex:
            print "Exception in unlinking /service/team3-frontend", ex

        print "Stopping existing service..."
        command = "svc -dx ."
        result = os.system(command)
        if result != 0:
            print "Failed to stop existing service - team3-frontend"
            sys.exit(1)
        command = "svc -dx log"
        result = os.system(command)
        if result != 0:
            print "Failed to stop existing service - team3-frontend"
            sys.exit(1)
    else:
        print "No existing service found for team3-frontend"

if __name__ == "__main__":
    main()
