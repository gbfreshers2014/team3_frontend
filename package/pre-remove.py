#! /usr/bin/env python
#
# Stops existing service if any
#

import os
import sys

def main():
    """Main function"""
    # Check if we have the version number as argument.
    if len(sys.argv) != 2:
        print "Usage: %s <version>" % (sys.argv[0])
        sys.exit(1)
    
    # Count of number of packages installed.
    num_packages = int(sys.argv[1])
    
    # Stop service if running.
    if num_packages == 0 and os.path.islink("/service/team3-frontend"):
        os.chdir("/service/team3-frontend")

        # Remove the symlink
        try:
            print "Unlinking /service/team3-frontend"
            os.unlink("/service/team3-frontend")
        except Exception as ex:
            print "Exception in unlinking /service/team3-frontend", ex

        print "Stopping existing service..."
        command = "svc -dx ."
        result = os.system(command)
        if result != 0:
            print "Failed to stop existing service - team3-frontend"
            sys.exit(1)
        command = "svc -dx log"
        result = os.system(command)
        if result != 0:
            print "Failed to stop existing service - team3-frontend/log"
            sys.exit(1)
    else:
        print "No existing service found for team3-frontend"

if __name__ == "__main__":
    main()
