#! /usr/bin/env python
#
# Does the following:
# - Creates the service link
# - Verifies that the service has started up
#

import os
import sys
import time

mojito_app_dir = "/home/gb/share/mojito/team3-frontend"

def main():
    """Main function"""

    print "Running post-install script..."

    # Check if we have the version number as argument.
    if len(sys.argv) != 2:
        print "Usage: %s <version>" % (sys.argv[0])
        sys.exit(1)
    
    # Version number.
    version = sys.argv[1]

    # Extract the zip file.
    os.chdir(mojito_app_dir)
    result = os.system("unzip -u -o app.zip")
    if result != 0:
        print "ERROR: Unable to extract app.zip in " + mojito_app_dir
        sys.exit(1)
    
    # Create the symlink.
    os.system("ln -snf /home/gb/service/team3-frontend-" + \
                version + " /service/team3-frontend")
    # Wait for svcscan to pick up the changes
    time.sleep(5)
    os.system("svc -u /service/team3-frontend/log")
    os.system("svc -u /service/team3-frontend")
    # Verify that the service is up.
    result = os.system("svstat /service/team3-frontend")
    if result != 0:
        print "ERROR: Service failed to start. Please verify"
        sys.exit(1)

    # Verify that the log service is up.
    result = os.system("svstat /service/team3-frontend/log")
    if result != 0:
        print "ERROR: Log service failed to start. Please verify"
        sys.exit(1)
        
if __name__ == "__main__":
    main()
